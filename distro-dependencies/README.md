When adding new packages, please keep the listing in alphabetical order.

If using JetBrains IDE, select the wanted lines, then in main menu: **Edit | Sort Lines**.
